package create_option;

import create_friend.CreateFriend;
import create_group.CreateGroup;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import login.Login;

public class CreateOptionController {

	@FXML
	private Button createGroup;
	
	@FXML
	private Button createFriend;
	
	@FXML
	private Button back;
	
	public void createGroup(ActionEvent event) {
		new CreateGroup().Show();
	}
	
	public void createFriend(ActionEvent event)
	{
		new CreateFriend().Show();
	}
	
	public void back(ActionEvent event)
	{
		new Login().Show();
	}
}
