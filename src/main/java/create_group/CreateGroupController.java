package create_group;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import homescreen.HomeScreen;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import signup.SignUp;

public class CreateGroupController {

	@FXML
	private TextField groupName;
	@FXML
	private TextField groupType;
	@FXML
	private Button cancle;
	@FXML
	private Button create;
	
	public void create(ActionEvent event) throws IOException
	{
		if (groupName.getText().isEmpty() || groupType.getText().isEmpty()) {
			Alert alert = new Alert(Alert.AlertType.ERROR);
			alert.setTitle("Error");
			alert.setHeaderText(null);
			alert.setContentText("Please fill in all fields.");
			alert.showAndWait();
			return;
		}


		final String messageContent = "{\n" + "\"groupName\"" + ":\"" + groupName.getText() + "\", \r\n" + "\"grouptype\"" + ":\""
				+ groupType.getText() +  "\" \r\n"+ "\n}";
		
		
		System.out.println(messageContent);

		String url = "http://localhost:8080/api/v1/createGroup";
		URL urlObj = new URL(url);
		HttpURLConnection postCon = (HttpURLConnection) urlObj.openConnection();
		postCon.setRequestMethod("POST");
		postCon.setRequestProperty("userId", "abcdef");
		
		postCon.setRequestProperty("Content-Type", "application/json");
		postCon.setDoOutput(true);
		
		OutputStream osObj = postCon.getOutputStream();
		osObj.write(messageContent.getBytes());
		osObj.flush();
		osObj.close();
		int respCode = postCon.getResponseCode();
		System.out.println("Response from the server is: \n");
		System.out.println("The POST Request Response Code :  " + respCode);
		System.out.println("The POST Request Response Message : " + postCon.getResponseMessage());
		if (respCode == HttpURLConnection.HTTP_CREATED) {
			
			InputStreamReader irObj = new InputStreamReader(postCon.getInputStream());
			BufferedReader br = new BufferedReader(irObj);
			String input = null;
			StringBuffer sb = new StringBuffer();
			while ((input = br.readLine()) != null) {
				sb.append(input);
			}
			br.close();
			postCon.disconnect();			
			System.out.println(sb.toString());
			new HomeScreen().Show();
			
		} else {
			System.out.println("POST Request did not work.");
			Alert alert = new Alert(Alert.AlertType.ERROR);
			alert.setTitle("Error");
			alert.setHeaderText(null);
			alert.setContentText("POST Request did not work.");
			alert.showAndWait();
			return;

			}

	}
	public void cancle(ActionEvent event)
	{
		new SignUp().Show();
	}
	
}
