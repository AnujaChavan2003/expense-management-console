package homescreen;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import login.Login;
import signup.SignUp;

public class HomeScreenController {

	@FXML
	private Button signUp;
	
	@FXML
	private Button login;
	
	public void signUp(ActionEvent event)
	{
	    new SignUp().Show();
	}
	
	public void login(ActionEvent event)
	{
		new Login().Show();
	}

}
