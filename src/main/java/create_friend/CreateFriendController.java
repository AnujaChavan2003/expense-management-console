package create_friend;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;

import create_option.CreateOption;
import homescreen.HomeScreen;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

public class CreateFriendController {

	@FXML
	private TextField fullName;
	
	@FXML
	private TextField email;
	
	@FXML
	private TextField mobile;
	
	@FXML
	private PasswordField password;
	
	@FXML
	private TextField currency;
	
	@FXML
	private TextField country;
	
	@FXML
	private TextField language;
	
	@FXML
	private Button cancle;
	
	@FXML
	private Button createFriend;
	
	
	public void cancle(ActionEvent event)
	{
		new HomeScreen().Show();
	}
	
	public void createFriend(ActionEvent event) throws IOException
	{
		
		final String messageContent = "{\n" + 
				"\"fullName\"" + ":\"" + fullName.getText() + "\", \r\n" +
				"\"email\"" + ":\"" + email.getText() + "\", \r\n" +
				"\"mobile\"" + ":\""+ mobile.getText() + "\", \r\n" + 
				"\"password\"" + ":\"" + password.getText() + "\", \r\n" +
				"\"country\"" + ":\"" + country.getText() + "\", \r\n" +
				"\"currency\"" + ":\""+ currency.getText() + "\", \r\n" + 
				"\"language\"" + ":\"" + language.getText() + "\" \r\n" +
				"\n}";

				
		       System.out.println(messageContent);

				String url = "http://localhost:8080/api/v1/create";
				URL urlObj = new URL(url);
				HttpURLConnection postCon = (HttpURLConnection) urlObj.openConnection();
				postCon.setRequestMethod("POST");
				postCon.setRequestProperty("userId", "abcdef");
				
				postCon.setRequestProperty("Content-Type", "application/json");
				postCon.setDoOutput(true);
				
				OutputStream osObj = postCon.getOutputStream();
				osObj.write(messageContent.getBytes());
				
				osObj.flush();
				osObj.close();
				int respCode = postCon.getResponseCode();
				System.out.println("Response from the server is: \n");
				System.out.println("The POST Request Response Code :  " + respCode);
				System.out.println("The POST Request Response Message : " + postCon.getResponseMessage());
				if (respCode == HttpURLConnection.HTTP_CREATED) {
					
					InputStreamReader irObj = new InputStreamReader(postCon.getInputStream());
					BufferedReader br = new BufferedReader(irObj);
					String input = null;
					StringBuffer sb = new StringBuffer();
					while ((input = br.readLine()) != null) {
						sb.append(input);
					}
					br.close();
					postCon.disconnect();
					
					System.out.println(sb.toString());
					new CreateOption().Show();
					
				} else {
					System.out.println("POST Request is Not Working.");
				}

	}
	
}
