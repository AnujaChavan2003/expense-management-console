package login;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Connection;
import java.sql.Statement;

import create_option.CreateOption;
import homescreen.HomeScreen;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

public class LoginController {

	static Connection con;
	static Statement stmt;
	
	@FXML
	private TextField email;
	
	@FXML
	private TextField mobile;
	
	@FXML
	private PasswordField password;
	
	@FXML
	private Button cancle;
	
	@FXML
	private Button login;
	
	
	public void login(ActionEvent event) throws IOException
	{
		final String messageContent = "{\n" + "\"email\"" + ":\"" + email.getText() + "\", \r\n" + "\"mobile\"" + ":\""
				+ mobile.getText() + "\", \r\n" + "\"password\"" + ":\"" + password.getText() + "\" \r\n" + "\n}";

		System.out.println(messageContent);

		String apiUrl = "http://localhost:8080/api/v1/validate";
		URL url = new URL(apiUrl);
		HttpURLConnection postConnection = (HttpURLConnection) url.openConnection();
		postConnection.setRequestMethod("POST");

		postConnection.setRequestProperty("Content-Type", "application/json");
		postConnection.setDoOutput(true);

		OutputStream outputStreamobj = postConnection.getOutputStream();
		outputStreamobj.write(messageContent.getBytes());

		outputStreamobj.flush();
		outputStreamobj.close();
		int respCode = postConnection.getResponseCode();
		System.out.println("Response from the server is: \n");
		System.out.println("The POST Request Response Code :  " + respCode);
		System.out.println("The POST Request Response Message : " + postConnection.getResponseMessage());
		if (respCode == HttpURLConnection.HTTP_CREATED) {

			InputStreamReader inputStreamObj = new InputStreamReader(postConnection.getInputStream());
			BufferedReader br = new BufferedReader(inputStreamObj);
			String input = null;
			StringBuffer stringBuffer = new StringBuffer();
			while ((input = br.readLine()) != null) {
				stringBuffer.append(input);
			}
			br.close();
			postConnection.disconnect();

			System.out.println(stringBuffer.toString());
			new CreateOption().Show();

		} else {
			System.out.println("POST Request did not work.");
			Alert alert = new Alert(Alert.AlertType.ERROR);
			alert.setTitle("Error");
			alert.setHeaderText(null);
			alert.setContentText("POST Request did not work.");
			alert.showAndWait();
			return;
		}

	}
	
	public void cancle(ActionEvent event)
	{
		new HomeScreen().Show();
	}

    }
