package application;

import homescreen.HomeScreen;
import javafx.application.Application;
import javafx.stage.Stage;
import stage_master.StageMaster;

public class ApplicationMain extends Application {
public static void main(String[] args) {
	launch(args);
}
	public void start(Stage primaryStage) {
		StageMaster.setStage(primaryStage);
		System.out.println("Application start");
		new HomeScreen().Show();
		System.out.println("Application stop");
	}
}


