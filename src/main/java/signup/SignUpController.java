package signup;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Connection;
import java.sql.Statement;

import create_option.CreateOption;
import homescreen.HomeScreen;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

public class SignUpController {

	static Connection con;
	static Statement stmt;
	
	@FXML
	private TextField fullName;
	
	@FXML
	private TextField email;
	
	@FXML
	private TextField mobile;
	
	@FXML
	private TextField currency;
	
	@FXML
	private TextField country;
	
	@FXML
	private PasswordField password;
	
	@FXML
	private TextField language;
	
	@FXML
	private Button cancle;
	
	@FXML
	private Button signUp;
	
	public void signUp(ActionEvent event) throws IOException
	{
		if (fullName.getText().isEmpty() || mobile.getText().isEmpty() || email.getText().isEmpty() || password.getText().isEmpty()) {
			Alert alert = new Alert(Alert.AlertType.ERROR);
			alert.setTitle("Error");
			alert.setHeaderText(null);
			alert.setContentText("Please fill in all fields.");
			alert.showAndWait();
			return;
		}
	                   	final String messageContent = "{\n" + 
												"\"fullName\"" + ":\"" + fullName.getText() + "\", \r\n" +
												"\"email\"" + ":\"" + email.getText() + "\", \r\n" +
												"\"mobile\"" + ":\""+ mobile.getText() + "\", \r\n" + 
												"\"password\"" + ":\"" + password.getText() + "\", \r\n" +
												"\"country\"" + ":\"" + country.getText() + "\", \r\n" +
												"\"currency\"" + ":\""+ currency.getText() + "\", \r\n" + 
												"\"language\"" + ":\"" + language.getText() + "\" \r\n" +
												"\n}";

				System.out.println(messageContent);

				String url = "http://localhost:8080/api/v1/signup";
				URL urlObj = new URL(url);
				HttpURLConnection postCon = (HttpURLConnection) urlObj.openConnection();
				postCon.setRequestMethod("POST");
				postCon.setRequestProperty("userId", "abcdef");

				postCon.setRequestProperty("Content-Type", "application/json");
				postCon.setDoOutput(true);

				OutputStream osObj = postCon.getOutputStream();
				osObj.write(messageContent.getBytes());

				osObj.flush();
				osObj.close();
				int respCode = postCon.getResponseCode();
				System.out.println("Response from the server is: \n");
				System.out.println("The POST Request Response Code :  " + respCode);
				System.out.println("The POST Request Response Message : " + postCon.getResponseMessage());
				if (respCode == HttpURLConnection.HTTP_CREATED) {
					
					InputStreamReader irObj = new InputStreamReader(postCon.getInputStream());
					BufferedReader br = new BufferedReader(irObj);
					String input = null;
					StringBuffer sb = new StringBuffer();
					while ((input = br.readLine()) != null) {
						sb.append(input);
					}
					br.close();
					postCon.disconnect();
					System.out.println(sb.toString());
					new CreateOption().Show();

				} else {
					System.out.println("POST Request did not work.");
					Alert alert = new Alert(Alert.AlertType.ERROR);
					alert.setTitle("Error");
					alert.setHeaderText(null);
					alert.setContentText("POST Request did not work.");
					alert.showAndWait();
					return;

				}

	}
	
	public void cancle(ActionEvent event) {
		new HomeScreen().Show();
	}
}
